# Nuclear Rain

**Made with <3 by [stdmatt](http://stdmatt.com).**

<!--  -->
## Description:

<p align="center">
    <img src="./res/nuclear_rain.gif"/>
</p>


A tribute to the classic and amazing Atari's Missle Command!

You can play it [here](https://stdmatt.com/deploy/games/nuclear_rain/index.html)

As usual, you are **very welcomed** to **share** and **hack** it.

<!--  -->
## Dependencies:

* [mcow_js_core](https://stdmatt.com/projects/libs/js/mcow_js_core.html)
* [mcow_js_canvas](https://stdmatt.com/projects/libs/js/mcow_js_canvas.html)

<!--  -->
## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

<!--  -->
## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
